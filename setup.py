from setuptools import setup, find_packages

setup(
    name='docentesipc',
    version='0.0.1',
    install_requires=[
        'requests',
        'importlib-metadata; python_version<"3.10"',
        'gspread >= 5.10.0',
        'pandas >= 1.5.3',
        'pyyaml >= 6.0',
        'qrcode >= 7.4.2',
        'validators >= 0.18.2',
        'python-crontab >= 3.0.0'
    ],
    packages=find_packages(
        where='.',
        include=['docentesipc*'],
        exclude=[],
    ),
)
