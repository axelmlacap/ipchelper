import pandas as pd
from pathlib import Path, WindowsPath, PosixPath
from datetime import datetime, timedelta
import random
import gspread
import logging
import typing as tp
import sys
import json
import qrcode
import matplotlib.pyplot as plt
import yaml
import validators
import stat
import shutil
from crontab import CronTab

KEYWORDS = {"frutas": ["anana", "arandano", "banana", "cereza", "ciruela", "frutilla", "higo", "kiwi", "lima", "limon",
                       "mandarina", "mandarina", "mango", "manzana", "mango", "manzana", "maracuya", "melon",
                       "membrillo", "mora", "palta", "pera", "platano", "quinoto", "sandia", "uva"]}


class InvalidEmail(Exception):
	def __init__(self, email, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.email = email


class InvalidDate(Exception):
	def __init__(self, date, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.date = date


class WrongTime(Exception):
	def __init__(self, time, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.time = time


class WrongKeyword(Exception):
	def __init__(self, keyword, classDate, classKeyword, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.keyword = keyword
		self.classDate = classDate
		self.classKeyword = classKeyword


class Course:
	"""Guía para habilitar el Asistente de Clase:

	1. Habilitar acceso de la API de Google:
		(Fuente: [documentación de GSpreads](https://docs.gspread.org/en/v6.0.0/oauth2.html#enable-api-access)
		1.1. Ir a [Google Developers Console](https://console.cloud.google.com/apis/dashboard) y hacer click en el
			botón '+ ENABLE APIS AND SERVICES'.
		1.2. En el recuadro de búsqueda 'Buscar APIs y servicios (Search for APIs and services)', buscar "Google Drive
			API” y habilitarlo.
		1.3. En el recuadro de búsqueda 'Buscar APIs y servicios (Search for APIs and services)', buscar "Google Sheets
			API” y habilitarlo.
		1.4. Crear una cuenta de servicio. Para ello, presionar el botón 'Crear credenciales (Create credential)' y
			elegir 'Cuenta de Servicio (Service Account)'. Llenar el formulario, presionar 'Crear (Create)' y 'Listo
			(Done)'.
        1.5. En la solapa 'Cuentas de servicio (Service Accounts)', ingresar a [Administrar Cuentas de Servicio (Manage Service Accounts)](https://console.cloud.google.com/iam-admin/serviceaccounts?)
        1.6. Crear una nueva clave. Para ello, en la columna 'Acciones (Actions)' presionar los tres puntos verticales
            (⋮) de la cuenta de servicio creada recientemente e ingresar a 'Administrar claves (Manage keys)'.
            Presionar 'Agregar clave (Add key)' > 'Crear clave nueva (Add new key)'. Elejir 'JSON' y presionar 'Crear
            (Create)'. Se descargará un archivo JSON con un contenido parecido a:
                ```{"type": "service_account",
                    "project_id": "api-project-XXX",
                    "private_key_id": "2cd … ba4",
                    "private_key": "-----BEGIN PRIVATE KEY-----\nNrDyLw … jINQh/9\n-----END PRIVATE KEY-----\n",
                    "client_email": "473000000000-yoursisdifferent@developer.gserviceaccount.com",
                    "client_id": "473 … hd.apps.googleusercontent.com",
                    ...}```
            Recordar la ubicación del archivo descargado: al inicializar el Asistente de Clase habrá que indicar esa
            ubicación como valor del argumento `credentialPath`. Además vamos a necesitar el valor del campo
            'client_email' de ese archivo en el paso siguiente.
		1.7. Desde Google Drive: compartir todos los archivos necesarios con la cuenta del campo 'client_email' del
			paso anterior. Caso contrario, recibiremos un error `gspread.exceptions.SpreadsheetNotFound`.
	"""

	_defaults = {"name": "Mi Curso",
	             "schedule": ["lun 8:00", "mar 8:00"],
	             "startDate": None,
	             "weeks": 20,
	             "classLength": 90,
	             "tolerance": 20,
	             "keywords": [],
	             "paths": {"props": None,
	                       "students": None,
	                       "log": None,
	                       "cred": None,
	                       "output": None,
	                       "input": None,
	                       "form": None},
	             "sheetNames": {"grades": "Calificaciones",
	                            "students": "Alumnos",
	                            "attendance": "Asistencia",
	                            "keywords": "Palabras clave"},
	             "colOrder": {"grades": ["A", "TP1", "TP2", "TP3", "p1", "P", "p2", "F", "GP", "R", "NdC"],
	                          "students": ["_lastName", "_firstName", "email", "_dni", "_degree", "_id", "_isathlete"],
	                          "input": ["timestamp", "email", "keyword", "result"],
	                          "keywords": ["date", "datetime", "keyword"]},
	             "timestampFmt": "%d/%m/%Y %H:%M:%S",
	             "dateFmt": "%d/%m/%y",
	             "classDates": [],
	             "classDatetimes": [],
	             "classKeywords": [],
	             "firstWeekDatetimes": [],
	             "attendanceInitPolicy": "byResponseDate",  # define cómo inicializar presentes de celdas vacías
	             "maxStudentCols": 3,  # define cuántas columnas se usarán para mostrar datos de los alumnos
	             "writeResultInInput": True,  # si almacena o no el resultado de cada presente en el archivo de respuestas
				 "nullAttendanceValue": "x"}  # con qué valor se indica que ese día no se tomó asistencia
	_logConfig = {'filemode': 'a',
	              'format': '[%(asctime)s] %(name)s (%(levelname)s): %(message)s',
	              'datefmt': '%d/%m/%Y %H:%M:%S',
	              'level': logging.INFO}
	_attendanceInitPolicyValues = ("byCurrentDate", "byResponseDate")
	_allowNewPaths = True

	def __init__(self,
	             name: str = "Mi curso",
	             schedule: tp.Sequence[str] = ("lun 8:00", "mar 8:00"),
	             startDate: str = None,
	             weeks: int = 20,
	             classLength: int = 90,
	             tolerance: int = 20,
	             keywords: tp.Union[str, Path, tp.List[str]] = "frutas",
	             paths: dict = None,
	             sheetNames: dict = None,
	             colOrder: dict = None,
	             timestampFmt: str = "%d/%m/%Y %H:%M:%S",
	             dateFmt: str = "%d/%m/%y",
	             students: tp.Union[pd.DataFrame, str, Path, None] = None,
	             **kwargs):
		self._props = dict(self._defaults)
		self._students = None
		self._client = None
		self.logger = logging.getLogger()
		self.name = name
		self.schedule = schedule
		self.startDate = startDate
		self.weeks = weeks
		self.classLength = classLength
		self.tolerance = tolerance
		self.keywords = keywords
		self.paths = paths if paths is not None else self._defaults["paths"]
		self.sheetNames = sheetNames if sheetNames is not None else self._defaults["sheetNames"]
		self.colOrder = colOrder if colOrder is not None else self._defaults["colOrder"]
		self.timestampFmt = timestampFmt
		self.dateFmt = dateFmt

		self.setProps(**kwargs)
		if students is not None:
			self.students = students
		elif self.paths["students"] is not None:
			self.students = self.paths["students"]

		self.initProps()
		self.initPaths()

	@property
	def props(self) -> dict:
		return self._props

	@props.setter
	def props(self, props: tp.Union[dict, str, Path]):
		if isinstance(props, (str, Path)):
			self.loadProps(path=props)
		elif isinstance(props, dict):
			self._props = props
		else:
			raise ValueError(f"Error asignando propiedad `courseProps`: el tipo de dato debe ser dict, str o "
			                 f"pathlib.Path. Se recibió: '{type(props)}'")

	@property
	def students(self) -> pd.DataFrame:
		return self._students

	@students.setter
	def students(self, students: tp.Union[pd.DataFrame, str, Path]):
		if isinstance(students, (str, Path)):
			self.loadStudents(students)
		elif isinstance(students, pd.DataFrame):
			self._students = students
			self.setPath("students", None)
		else:
			raise ValueError(f"Error asignando propiedad `courseStudents`: el tipo de dato debe ser pandas.DataFrame, "
			                 f"str o pathlib.Path. Se recibió: '{type(students)}'")

	@property
	def name(self) -> str:
		return self.props["name"]

	@name.setter
	def name(self, value: str):
		self.props["name"] = str(value)

	@property
	def schedule(self) -> tp.List[str]:
		return self.props["schedule"]

	@schedule.setter
	def schedule(self, value: tp.List[str]):
		self.props["schedule"] = [str(v) for v in value]

	@property
	def startDate(self) -> str:
		return self.props["startDate"]

	@startDate.setter
	def startDate(self, value: str):
		if value is None:
			value = datetime.today().strftime(self.dateFmt)
		self.props["startDate"] = str(value)

	@property
	def weeks(self) -> int:
		return self.props["weeks"]

	@weeks.setter
	def weeks(self, value):
		self.props["weeks"] = int(value)

	@property
	def classLength(self) -> int:
		return self.props["classLength"]

	@classLength.setter
	def classLength(self, value):
		self.props["classLength"] = int(value)

	@property
	def tolerance(self) -> int:
		return self.props["tolerance"]

	@tolerance.setter
	def tolerance(self, value):
		self.props["tolerance"] = int(value)

	@property
	def keywords(self) -> tp.Union[tp.List[str], None]:
		return self.props["keywords"]

	@keywords.setter
	def keywords(self, value: tp.Union):
		if value is None:
			self.props["keywords"] = []
		elif isinstance(value, str) and value in KEYWORDS:
			self.props["keywords"] = KEYWORDS[value]
		else:
			if validators.url(str(value)) or Path(value).suffix.lower() in [".csv", ".xls", ".xlsx"]:
				self.loadKeywords(value)
			else:
				path = Path(value).resolve()
				with open(path, "r") as file:
					self.props["keywords"] = [kwd.strip() for kwd in file.readlines() if kwd.strip() != ""]

	@property
	def paths(self) -> tp.Dict[str, tp.Union[Path, None]]:
		return self._props["paths"]

	@paths.setter
	def paths(self, value: tp.Dict[str, tp.Union[str, Path, None]]):
		for which, path in value.items():
			self.setPath(which, path)

	@property
	def path(self) -> Path:
		return self.paths["props"]

	@path.setter
	def path(self, value):
		self.setPath("props", value)

	@property
	def logPath(self) -> tp.Union[Path, None]:
		return self.paths["log"]

	@logPath.setter
	def logPath(self, value: tp.Union[str, Path, None]):
		self.setPath("log", value)

	@property
	def credentialPath(self) -> Path:
		return self.paths["cred"]

	@credentialPath.setter
	def credentialPath(self, value: tp.Union[str, Path, None]):
		self.setPath("cred", value)

	@property
	def sheetNames(self) -> tp.Dict[str, str]:
		return self.props["sheetNames"]

	@sheetNames.setter
	def sheetNames(self, value: tp.Dict[str, str]):
		self.props["sheetNames"].update(value)

	@property
	def colOrder(self) -> tp.Dict[str, tp.List[str]]:
		return self.props["colOrder"]

	@colOrder.setter
	def colOrder(self, value: tp.Dict[str, tp.List[str]]):
		self.props["colOrder"].update(value)

	@property
	def timestampFmt(self) -> str:
		return self.props["timestampFmt"]

	@timestampFmt.setter
	def timestampFmt(self, value: str):
		self.props["timestamFmt"] = str(value)

	@property
	def dateFmt(self) -> str:
		return self.props["dateFmt"]

	@dateFmt.setter
	def dateFmt(self, value: str):
		self.props["dateFmt"] = str(value)

	@property
	def client(self) -> gspread.Client:
		if self._client is None:
			self.authenticate()

		return self._client

	@classmethod
	def _loadYaml(cls, path: tp.Union[str, Path]) -> dict:
		path = Path(path).resolve()
		with open(path, "r") as file:
			return yaml.load(file, yaml.Loader)

	@classmethod
	def _saveYaml(cls, data: dict, path: tp.Union[str, Path]) -> Path:
		data = dict(data)
		path = Path(path).resolve()
		with open(path, "w") as file:
			yaml.dump(data, file, yaml.Dumper, default_flow_style=None)
		return path

	@classmethod
	def _loadJson(cls, path: tp.Union[str, Path]) -> dict:
		path = Path(path).resolve()
		with open(path, "r") as file:
			return json.load(file)

	@classmethod
	def _saveJson(cls, data: dict, path: tp.Union[str, Path]) -> Path:
		path = Path(path).resolve()
		with open(path, "w") as file:
			json.dump(data, file)
		return path

	def _loadSheet(self, path: tp.Union[str, Path], sheetName: str = None, delimiter: str = ",",
	               dateCols: tp.Sequence[int] = (), renderOption: str = "FORMATTED_VALUE",
	               *args, **kwargs) \
			-> pd.DataFrame:
		if validators.url(str(path)):
			inputFile = self.client.open_by_url(str(path))
			if sheetName is None:
				inputWorksheet = inputFile.get_worksheet(0)
			else:
				inputWorksheet = inputFile.worksheet(sheetName)
			return pd.DataFrame(inputWorksheet.get_all_records(value_render_option=renderOption, *args, **kwargs))
		else:
			path = Path(path).resolve()
			if path.suffix.lower() in [".xls", ".xlsx"]:
				if sheetName is None:
					sheetName = 0
				df = pd.read_excel(path, sheetName, keep_default_na=False, *args, **kwargs)
				for col in dateCols:
					colName = df.columns[col]
					df[colName] = [x.strftime(self.timestampFmt) if hasattr(x, "strftime") else str(x) for x in df[colName]]
				return df
			elif path.suffix.lower() == ".csv":
				return pd.read_csv(path, sep=delimiter, *args, **kwargs)
			else:
				raise ValueError(
					f"Error abriendo hoja de datos: solo se pueden abrir archivos de Excel (.xls, .xlsx) y "
					f"CSV (.csv). Se recibió: '{path}'.")

	def _saveSheet(self, data: tp.Union[dict, pd.DataFrame], path: tp.Union[str, Path], sheetName: str = None,
	               delimiter: str = ",", index=None, columns=None, inputOption: str = "USER_ENTERED",
	               formatFirstRow: bool = False, *args, **kwargs) \
			-> tp.Union[Path, None]:
		if isinstance(data, dict):
			data = pd.DataFrame(data, index=index, columns=columns)

		if validators.url(str(path)):
			path = str(path)
			file = self.client.open_by_url(path)
			if sheetName is None:
				worksheet = file.get_worksheet(0)
			else:
				worksheetNames = [ws.title for ws in file.worksheets()]
				if sheetName not in worksheetNames:
					file.add_worksheet(sheetName, *data.shape)
				worksheet = file.worksheet(sheetName)
			worksheet.clear()
			headerRange = "A1:" + gspread.utils.rowcol_to_a1(1, worksheet.col_count + 1)
			bodyRange = "A2:" + gspread.utils.rowcol_to_a1(worksheet.row_count + 2, worksheet.col_count + 2)
			worksheet.update(headerRange, [data.columns.values.tolist()], value_input_option="RAW")
			worksheet.update(bodyRange, data.values.tolist(), value_input_option=inputOption)
			# worksheet.update([data.columns.values.tolist()] + data.values.tolist(), value_input_option=inputOption,
			#                  *args, **kwargs)
		else:
			path = Path(path).resolve()
			if path.suffix.lower() in [".xls", ".xlsx"]:
				if path.exists():
					mode, if_sheet_exists = "a", "replace"
				else:
					mode, if_sheet_exists = "w", None
				with pd.ExcelWriter(path, mode=mode, if_sheet_exists=if_sheet_exists) as writer:
					if sheetName is None:
						sheetName = list(writer.sheets)[0]
					data.to_excel(writer, sheetName, index=False, *args, **kwargs)
			elif path.suffix.lower() == ".csv":
				data.to_csv(path, delimiter, index=False, *args, **kwargs)
			else:
				raise ValueError(f"La extensión '{path.suffix}' no es un formato de hoja de datos válido.")

		return path

	def authenticate(self, credentialPath: tp.Union[str, Path] = None) -> gspread.Client:
		if credentialPath is not None:
			self.credentialPath = credentialPath
		if self.credentialPath is None:
			raise IOError(f"Error al autenticar API de Google Drive: no se especificó ninguna credencial.")
		self._client = gspread.service_account(self.credentialPath)

		return self._client

	def setProps(self, props: dict = None, **kwargs) -> dict:
		if props is None:
			props = {}
		props.update(kwargs)

		for prop, value in props.items():
			if prop == "paths":
				self.paths = value
			elif prop in self._props:
				self._props[prop] = value

		return self.props

	def setPath(self, which: str, path: tp.Union[str, Path, None], passThrough: bool = False) -> tp.Union[Path, None]:
		# Verifico si es un tipo de ruta permitido/existente
		if not self._allowNewPaths and which not in self.paths:
			# raise ValueError(f"No se puede asignar la ruta '{which}': no se admiten rutas más allá de las definidas en "
			#                  f"la propiedad `paths` ({', '.join(self.paths.keys())}).")
			return None

		# Convierto tipo de dato
		if path is None:
			pass
		elif validators.url(str(path)):
			path = str(path)
		else:
			path = Path(path).resolve()

		# Verifico si la ruta se modifica
		if which in self.paths and path == self.paths[which]:
			return self.paths[which]

		# Valido el valor
		if which == "props":
			if path.suffix.lower() not in [".json", ".yaml"]:
				raise ValueError(f"El archivo debe tener extensión '.json' ó '.yaml'.")
		elif which == "cred":
			if not path.exists():
				raise ValueError(f"Error asignando la ruta {which}: no existe el archivo '{path}'.")
			if not path.suffix.lower() == ".json":
				raise ValueError(
					f"Error asignando la ruta '{which}': el archivo debe corresponder a una credencial "
					f"de Google API con extensión '.json'. Se recibió: '{path}'.")
		elif which in ["students", "output", "input", "campus", "gestion", "keywords"]:
			if isinstance(path, Path) and path.suffix.lower() not in [".csv", ".xls", ".xlsx"]:
				raise ValueError(f"Error asignando la ruta '{which}': el archivo debe ser formato '.csv', '.xls' ó "
				                 f"'.xlsx'. Se recibió: '{path}'.")

		# Actualizo el valor
		if not passThrough:
			self.paths[which] = path
			if which in ["props", "log"]:
				self.initPaths()

		return path

	def initProps(self, saveTo: tp.Union[str, Path] = None) -> dict:
		"""Genera un diccionario con las propiedades que especifican las clases del curso durante el semestre.

		Args:
			saveTo (str, pathlib.Path; opcional): ruta a un archivo YAML donde se guardará el diccionario de
				salida. Por defecto: no escribe ningún archivo de salida.
		"""
		# Genera lista de datetimes (fecha y hora) correspondientes a las clases de la primera semana
		startDatetime = datetime.strptime(self.startDate, self.dateFmt)  # Fecha de inicio del semestre
		if startDatetime.weekday() != 0:
			# Si el día de inicio ingresado no corresponde a un lunes, reemplaza el día por el lunes de esa semana
			startDatetime -= timedelta(startDatetime.weekday())
		firstWeekDatetimes = []
		for sch in self.schedule:
			classWeekDay, classTime = sch.split(" ")
			startH, startM = classTime.split(":")
			classWeekDay = ["lun", "mar", "mie", "jue", "vie", "sab", "dom"].index(classWeekDay.lower()[0:3])
			classDatetime = startDatetime + timedelta(days=classWeekDay, hours=int(startH), minutes=int(startM))
			firstWeekDatetimes.append(classDatetime)
		self.props["firstWeekDatetimes"] = firstWeekDatetimes

		# Genera lista de fechas y datetimes de todas las clases
		classDates = []
		classDatetimes = []
		for i in range(len(firstWeekDatetimes) * self.weeks):
			weekNumber = i // len(firstWeekDatetimes)
			classDatetime = firstWeekDatetimes[i % len(firstWeekDatetimes)] + timedelta(weeks=weekNumber)
			classDate = classDatetime.strftime(self.dateFmt)
			classDatetimes.append(classDatetime)
			classDates.append(classDate)
		self.props["classDates"] = classDates
		self.props["classDatetimes"] = classDatetimes

		# Genera lista de palabras claves
		if len(self.props["classKeywords"]) == 0:
			self.props["classKeywords"] = random.choices(self.keywords, k=len(classDates))

		# Escribe a archivo
		if saveTo is not None:
			self.saveProps(self.path)

		return self.props

	def loadProps(self, path: tp.Union[str, Path]) -> dict:
		path = self.setPath("props", path)
		with open(path, "r") as file:
			if path.suffix.lower() == ".json":
				props = self._loadJson(path)
			elif path.suffix.lower() == ".yaml":
				props = self._loadYaml(path)

		# Convierto str a datetime
		for prop in ["classDatetimes", "firstWeekDatetimes"]:
			if prop in props:
				props[prop] = [datetime.strptime(c, self.timestampFmt) for c in props[prop]]

		# Separo los paths para cargar por separado
		if "paths" in props:
			paths = props.pop("paths")
			for which in paths:
				if paths[which] == "None":
					paths[which] = None
		else:
			paths = None

		# Actualizo valores
		self._props.update(props)
		if paths is not None:
			paths["props"] = path
			self.paths = paths
		else:
			self.path = path

		return props

	def saveProps(self, path: tp.Union[str, Path]) -> Path:
		if self.path is None:
			path = self.setPath("props", path)
		else:
			path = self.setPath("props", path, passThrough=True)

		propsAux = dict(self.props)
		propsAux["classDatetimes"] = [c.strftime(self.timestampFmt) for c in self.props['classDatetimes']]
		propsAux["firstWeekDatetimes"] = [c.strftime(self.timestampFmt) for c in self.props['firstWeekDatetimes']]
		propsAux["paths"] = {k: str(v) for k, v in self.paths.items()}

		if path.suffix.lower() == ".json":
			self._saveJson(propsAux, path)
		elif path.suffix.lower() == ".yaml":
			self._saveYaml(propsAux, path)

		return self.path

	def initPaths(self, paths: dict = None):
		if paths is not None:
			self.paths = paths

		# Setup log:
		if self.paths["log"] is None and self.path is None:
			logging.basicConfig(stream=sys.stderr,
			                    **self._logConfig)
		else:
			if self.paths["log"] is None:
				self.paths["log"] = self.path.parent / f"{self.path.stem}.log"
			logging.basicConfig(filename=self.paths["log"],
			                    **self._logConfig)

	def initStudents(self,
	                 gestion: tp.Union[str, Path],
	                 campus: tp.Union[str, Path, None] = None,
	                 saveTo: tp.Union[str, Path] = None) -> pd.DataFrame:
		gestion = Path(gestion).resolve()
		studentsDf = self._loadSheet(gestion).iloc[:, 1:]
		studentsDf["Apellido"] = [s.title() for s in studentsDf["Apellido"]]
		studentsDf["Nombre"] = [s.title() for s in studentsDf["Nombre"]]
		studentsDf = studentsDf.sort_values(by="Apellido")

		students = {"Apellido": studentsDf["Apellido"],
		            "Nombre": studentsDf["Nombre"],
		            "Correo electrónico": [""] * studentsDf.shape[0]}

		if campus is not None:
			campus = Path(campus).resolve()
			campusDf = self._loadSheet(campus)

			if campusDf.shape[0] != studentsDf.shape[0]:
				raise ValueError(f"Las tablas `studentTable` y `CampusTable` no tienen la misma cantidad de filas "
				                 f"({studentsDf.shape[0]} y {campusDf.shape[0]} respectivamente).")

			campusDf["Apellido(s)"] = [s.title() for s in campusDf["Apellido(s)"]]
			campusDf["Nombre"] = [s.title() for s in campusDf["Nombre"]]
			campusDf = campusDf.sort_values(by="Apellido(s)")
			students["Correo electrónico"] = campusDf["Dirección de correo"]

		students = pd.DataFrame(students)
		if "Correo electrónico" in students and not any(students["Correo electrónico"] == ""):
			students.index = students["Correo electrónico"]

		self.students = students
		if saveTo is not None:
			self.saveStudents(saveTo)

		return students

	def loadStudents(self, path: tp.Union[str, Path], *args, **kwargs) -> pd.DataFrame:
		path = self.setPath("students", path)
		students = self._loadSheet(path, *args, **kwargs)
		students.index = students.iloc[:, self.colOrder["students"].index("email")]
		self._students = students

		return self.students

	def saveStudents(self, path: tp.Union[str, Path], *args, **kwargs) -> Path:
		if self.paths["students"] is None:
			path = self.setPath("students", path)
		else:
			path = self.setPath("students", path, passThrough=True)
		self._saveSheet(self.students, path, *args, **kwargs)

		return path

	def loadKeywords(self, path: tp.Union[str, Path], sheetName: tp.Union[str, None] = None, *args, **kwargs) -> tp.List[str]:
		path = self.setPath("keywords", path)

		if sheetName is None:
			sheetName = self.sheetNames["keywords"]
		else:
			self.sheetNames["keywords"] = sheetName

		data = self._loadSheet(path, sheetName)
		classKeywords = data.iloc[:, self.colOrder["keywords"].index("keyword")]
		self.props["classKeywords"] = classKeywords
		self.props["keywords"] = list(set(classKeywords))
		self.props["keywords"].sort()

		return classKeywords

	def saveKeywords(self, path: tp.Union[str, Path], sheetName: tp.Union[str, None] = None) -> Path:
		if self.paths["keywords"] is None:
			path = self.setPath("keywords", path)
		else:
			path = self.setPath("keywords", path, passThrough=True)

		if sheetName is None:
			sheetName = self.sheetNames["keywords"]
		else:
			self.sheetNames["keywords"] = sheetName

		keywords = {"date": self.props["classDates"],
		            "datetime": [c.strftime(self.timestampFmt) for c in self.props["classDatetimes"]],
		            "keyword": self.props["classKeywords"]}
		keywords = pd.DataFrame(keywords)
		self._saveSheet(keywords, path, sheetName)

	def createSheets(self, sheetNames: dict = None, colOrder: dict = None, saveTo: tp.Union[str, Path, None] = None) \
			-> tp.Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame, pd.DataFrame]:
		if sheetNames is not None:
			self.sheetNames = sheetNames
		if colOrder is not None:
			self.colOrder = colOrder

		nStudents = self.students.shape[0]
		nClasses = len(self.props["classDates"])

		# Genera DataFrame para la hoja de calificaciones
		grades = self.students.iloc[:, 0:self.props['maxStudentCols']].copy()
		for colName in self.colOrder["grades"]:
			grades[colName] = [""] * nStudents
		grades[self.colOrder["grades"][0]] = [f"={self.sheetNames['attendance']}!{gspread.cell.rowcol_to_a1(i + 2, 4)}"
		                                      for i in range(nStudents)]

		# Genera DataFrame para la hoja de asistencia
		attendance = self.students.iloc[:, 0:self.props['maxStudentCols']].copy()

		# Agrega columna de asistencia (con cálculo automático)
		attendanceCol = []
		for i in range(nStudents):
			rowcolFrom = gspread.cell.rowcol_to_a1(i + 2, 5)
			rowcolTo = gspread.cell.rowcol_to_a1(i + 2, 5 + nClasses)
			attendanceCol.append(
				f"=SUMA({rowcolFrom}:{rowcolTo}) / SUMA(ARRAYFORMULA(SI(ESNUMERO({rowcolFrom}:{rowcolTo})=VERDADERO; 1; 0)))")
		attendance["Asistencia"] = attendanceCol

		# Agrega columnas para cada fecha de clase
		for classDate in self.props["classDates"]:
			attendance[classDate] = [""] * self.students.shape[0]

		attendance = pd.DataFrame(attendance)

		# Genera DataFrame para la hoja de keywords
		keywords = {"date": self.props["classDates"],
		            "datetime": [c.strftime(self.timestampFmt) for c in self.props["classDatetimes"]],
		            "keyword": self.props["classKeywords"]}
		keywords = pd.DataFrame(keywords)

		# Guardo en el archivo
		if saveTo is not None:
			self.setPath("output", saveTo)

		if self.paths["output"] is not None:
			if "grades" in self.sheetNames:
				self._saveSheet(grades, self.paths["output"], self.sheetNames["grades"])
			if "attendance" in self.sheetNames:
				self._saveSheet(attendance, self.paths["output"], self.sheetNames["attendance"])
			if "students" in self.sheetNames:
				self._saveSheet(self.students, self.paths["output"], self.sheetNames["students"])
			if "keywords" in self.sheetNames:
				self._saveSheet(keywords, self.paths["output"], self.sheetNames["keywords"])

		return grades, attendance, self.students, keywords

	def createQrSlides(self, saveTo: tp.Union[str, Path] = None):
		if saveTo is None:
			if "qrSlides" in self.paths and self.paths["qrSlides"] is not None:
				saveTo = self.paths["qrSlides"]
			elif self.path is not None:
				saveTo = self.path.parent / "qrSlides/"
			else:
				saveTo = Path(f"qrSildes/").resolve()
		else:
			saveTo = Path(saveTo).resolve()
		self.paths["qrSlides"] = saveTo

		if not saveTo.exists():
			saveTo.mkdir(parents=True)

		qrImage = qrcode.make(str(self.paths["form"]))
		figResolution = 1080  # [px]
		figDpi = 72  # [px/in]
		figAspect = 16 / 9  # [1]
		figHeight = figResolution / figDpi  # [in]
		weekDays = ["lunes", "martes", "miércoles", "jueves", "viernes", "sábado", "domingo"]
		months = ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre",
		          "noviembre", "diciembre"]

		for classDate, classDatetime, keyword in zip(self.props["classDates"], self.props["classDatetimes"],
		                                             self.props["classKeywords"]):
			classDatetime: datetime
			weekDay = weekDays[classDatetime.weekday()]
			day = classDatetime.day
			month = months[classDatetime.month - 1]
			title = f"{weekDay.title()} {classDatetime.day} de {month}"

			fig: plt.Figure = plt.figure(figsize=(figHeight * figAspect, figHeight), dpi=figDpi)
			axHeight = 0.6
			axWidth = axHeight / figAspect
			ax: plt.Axes = fig.add_axes([0.5 - axWidth / 2, 0.5 - axHeight / 2, axWidth, axHeight])
			ax.imshow(qrImage, cmap="gray")
			ax.text(0.5, 1.1, title, transform=ax.transAxes, fontsize=100, ha="center", va="center",
			        fontweight="bold")
			ax.text(0.5, -0.1, f"Palabra clave: {keyword}", transform=ax.transAxes, fontsize=70, ha="center",
			        va="center")
			ax.set_axis_off()

			fileName = f"{classDatetime.strftime('%y%m%d')}_{weekDay[0:3]}{day}{month[0:3]}.png"
			fig.savefig(saveTo / fileName, dpi=figDpi)
			plt.close(fig)

	def takeAttendance(self):
		logging.info(f"Iniciando procesamiento de asistencia...")

		# Abro archivos
		inputDataframe = self._loadSheet(path=self.paths["input"], dateCols=[self.colOrder["input"].index("timestamp")])
		outputDataframe = self._loadSheet(path=self.paths["output"], sheetName=self.sheetNames["attendance"],
		                                  renderOption="FORMULA", numericise_ignore=["all"])
		outputDataframe.index = outputDataframe["Correo electrónico"]

		# Inicializo columnas
		if self.props["attendanceInitPolicy"] == "byCurrentDate":
			todayDatetime = datetime.today()
			for classDatetime, classDate in zip(self.props["classDatetimes"], self.props["classDates"]):
				if classDatetime < todayDatetime and any(outputDataframe[classDate] == ""):
					logging.info(f"Inicializando presentes de la fecha '{classDate}'...")
					outputDataframe.loc[outputDataframe[classDate] == "", classDate] = 0

		# Proceso respuestas
		for i, response in inputDataframe.iterrows():
			responseTimestamp = response.iloc[self.colOrder["input"].index("timestamp")]
			responseEmail = response.iloc[self.colOrder["input"].index("email")]
			responseKeyword = response.iloc[self.colOrder["input"].index("keyword")]
			if self.props["writeResultInInput"]:
				responseResult = response.iloc[self.colOrder["input"].index("result")]
				# Salteo respuestas procesadas
				if responseResult != "":
					logging.info(f"Salteando respuesta nro. {i}: la columna 'Resultado' con valor {responseResult} no está "
					             f"vacía.")
					continue

			# Inicializo columnas
			responseDatetime = datetime.strptime(responseTimestamp, self.timestampFmt)
			responseDate = responseDatetime.strftime(self.dateFmt)
			if self.props["attendanceInitPolicy"] == "byResponseDate":
				for classDatetime, classDate in zip(self.props["classDatetimes"], self.props["classDates"]):
					if classDate == responseDate and any(outputDataframe[classDate] == ""):
						logging.info(f"Inicializando presentes de la fecha '{classDate}' a 0...")
						outputDataframe.loc[outputDataframe[classDate] == "", classDate] = 0

			# Proceso respuesta
			result, exc = self.validateResponse(responseTimestamp, responseEmail, responseKeyword)
			if exc is None:
				pass
			elif isinstance(exc, InvalidEmail):
				logging.warning(f"Respuesta inválida en fila nro. {i}: email '{exc.email}' desconocido.")
			elif isinstance(exc, InvalidDate):
				logging.info(f"Respuesta inválida en fila nro. {i}: la fecha de respuesta '{exc.date}' "
				             f"no es una fecha de clase.")
			elif isinstance(exc, WrongTime):
				logging.info(f"Respuesta inválida en fila nro. {i}: la hora de la respuesta '{exc.time}' "
				             f"no se halla dentro del rango válido.")
			elif isinstance(exc, WrongKeyword):
				logging.info(f"Respuesta inválida en fila nro. {i}: la palabra clave '{exc.keyword}' no "
				             f"corresponde a la palabra clave '{exc.classKeyword}' de la clase '{exc.classDate}'.")

			# Actualizo valor
			outputDataframe.loc[responseEmail, responseDate] = result
			if self.props["writeResultInInput"]:
				inputDataframe.iloc[i, self.colOrder["input"].index("result")] = result
			logging.info(
				f"Se asignó {'presente' if result else 'ausente'} al alumno '{responseEmail}' el día '{responseDate}'.")

		# Relleno columnas vacías
		if self.props["nullAttendanceValue"] is not None:
			todayDatetime = datetime.today()
			for classDatetime, classDate in zip(self.props["classDatetimes"], self.props["classDates"]):
				if classDatetime < todayDatetime and any(outputDataframe[classDate] == ""):
					logging.info(f"Inicializando presentes de la fecha '{classDate}'...")
					outputDataframe.loc[outputDataframe[classDate] == "", classDate] = self.props["nullAttendanceValue"]

		# Escribo worksheet
		if self.props["writeResultInInput"]:
			self._saveSheet(inputDataframe, self.paths["input"], inputOption="USER_ENTERED")
		self._saveSheet(outputDataframe, self.paths["output"], self.sheetNames["attendance"], inputOption="USER_ENTERED")
		logging.info(f"Finalizado satisfactoriamente.")

	def validateResponse(self, timestamp: str, email: str, keyword: str) -> tp.Tuple[int, tp.Union[None, Exception]]:
		# Valido al email de la respuesta
		if email not in self.students.iloc[:, self.colOrder["students"].index("email")]:
			return 0, InvalidEmail(email)

		# Valido la fecha de la respuesta
		responseDatetime = datetime.strptime(timestamp, self.timestampFmt)
		responseDate = responseDatetime.strftime(self.dateFmt)
		if responseDate not in self.props["classDates"]:
			return 0, InvalidDate(responseDate)
		classIndex = self.props["classDates"].index(responseDate)

		# Valido la hora de la respuesta
		classDatetime = self.props["classDatetimes"][classIndex]
		timeCondition = timedelta(minutes=-self.tolerance) < responseDatetime - classDatetime < timedelta(
			minutes=self.classLength + self.tolerance)
		if not timeCondition:
			return 0, WrongTime(responseDatetime)

		# Condicion de palabra clave
		classKeyword = self.props['classKeywords'][classIndex]
		keywordCondition = keyword == classKeyword
		if not keywordCondition:
			return 0, WrongKeyword(keyword, responseDate, classKeyword)

		return 1, None

	def makeScript(self, saveTo: tp.Union[str, Path], crontab: bool = False) -> Path:
		saveTo = Path(saveTo).resolve().with_suffix(".py")

		try:
			path1 = self.paths['props'].relative_to(saveTo.parent)
		except ValueError:
			path1 = self.path['props']
		try:
			path2 = self.paths['students'].relative_to(saveTo.parent)
		except ValueError:
			path2 = self.path['students']

		script = f"#!{sys.executable}\n" \
		         f"from docentesipc import Course\n" \
		         f"course = Course.load('{path1}', '{path2}')\n" \
		         f"course.takeAttendance()\n"
		with open(saveTo, "w") as file:
			file.write(script)
		if isinstance(saveTo, PosixPath):
			saveTo.chmod(saveTo.stat().st_mode | stat.S_IEXEC)

		if crontab:
			if sys.platform not in ["darwin", "linux"]:
				raise NotImplementedError(
					f"Error generando script: la funcionalidad `crontab` no está implementada en Windows.")
			cron = CronTab(user=True)
			for cronTime in self.getCrontabDatetimes():
				job = cron.new(command=str(saveTo))
				job.setall(cronTime)
			cron.write()

		return saveTo

	def getCrontabDatetimes(self) -> tp.Tuple[str, ...]:
		times = []
		for classDatetime in self.props["firstWeekDatetimes"]:
			endDatetime = classDatetime + timedelta(minutes=self.classLength + self.tolerance + 10)
			time = f"{endDatetime.minute} {endDatetime.hour} * * {endDatetime.weekday()}"
			times.append(time)

		return tuple(times)

	def save(self, path: tp.Union[str, Path]):
		path = Path(path).resolve()
		if not path.is_dir():
			raise ValueError(f"Error guardando curso: el argumento `path` debe ser la ruta a un directorio, no a un "
			                 f"archivo. Se recibió: '{path}'.")
		if not path.exists():
			path.mkdir()
		if self.paths["cred"] is not None:
			srcPath = self.paths["cred"]
			dstPath = path / srcPath.name
			try:
				shutil.copy(str(srcPath), str(dstPath))
			except shutil.SameFileError:
				pass
			self.setPath("cred", dstPath)
		self.setPath("students", path / "students.csv")
		self.setPath("props", path / "props.yaml")
		self.saveStudents(self.paths["students"])
		self.saveProps(self.paths["props"])

	@classmethod
	def load(cls, props: tp.Union[str, Path], students: tp.Union[str, Path]):
		obj = cls()
		obj.loadProps(props)
		obj.loadStudents(students)

		return obj

	@classmethod
	def create(cls, initFile: tp.Union[str, Path], saveTo: tp.Union[str, Path] = ".", createSheets: bool = True,
	           createQrSlides: bool = True, makeScript: bool = True, crontab: bool = False, **kwargs):
		saveTo = Path(saveTo).resolve()
		if not saveTo.exists():
			saveTo.mkdir()
		if crontab is None:
			crontab = sys.platform in ["linux", "darwin"]

		initFile = Path(initFile).resolve()
		initKwargs = cls._loadYaml(initFile)
		initKwargs.update(kwargs)
		paths = initKwargs["paths"] if "paths" in initKwargs else {}
		obj = cls(**initKwargs)

		if obj.students is None:
			if "gestion" in paths:
				gestion = paths["gestion"]
				campus = paths["campus"] if "campus" in paths else None
				obj.initStudents(gestion, campus, saveTo=saveTo / f"students.csv")

		if createSheets:
			if obj.paths["output"] is None:
				obj.setPath("output", saveTo / f"{obj.name}.xlsx")
			obj.createSheets()

		if createQrSlides and "form" in obj.paths and len(obj.props["classKeywords"]) > 0:
			obj.createQrSlides(saveTo=saveTo / "qrSlides/")

		if saveTo is not None:
			saveTo = Path(saveTo).resolve()
			obj.save(saveTo)
			if makeScript:
				obj.makeScript(saveTo / "pasarLista.py", crontab)

		return obj
