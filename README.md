# Asistente de IPC

API para docentes de **UdeSA** de la materia **Introducción al Pensamiento 
Computacional**.

## Funcionalidades


### Pasar lista de forma digital

Durante la clase se muestra a los alumnos un código QR junto a una palabra clave que va cambiando cada día. 

![](docs/readme-qr.png)

El código QR lleva a un formulario de Google que los alumnos deben responder ingresando la palabra clave de ese día. 
La respuesta debe registrarse durante el horario de clase y con la cuenta _@udesa.edu.ar_ del alumno.

![](docs/readme-form.png)

Las respuestas del formulario se almacenan en una hoja de datos de Google en la carpeta de Drive de cada curso.

![](docs/readme-input.png)

La API lee las respuestas y llena los presentes en la planilla del curso automáticamente: aquellos que respondieron con 
un email válido, en horario de clase e ingresaron la palabra clave correcta tendrán _presente_ (1), y si no, ausente 
(0).

![](docs/readme-output.png)


### Crear planilla del curso automáticamente

La API lee las listas de alumnos generadas por el sistema de Gestión y el Campus Virtual y crea automáticamente una 
hoja de datos con la planilla del curso para volcar las calificaciones de los alumnos. También genera hojas de datos 
para almacenar la asistencia, las listas de alumnos y de palabras claves.

![](docs/readme-grades.png)


### Trabajar con archivos locales o en la nube

Las hojas de datos se pueden guardar en el almacenamiento local en formato Excel (_.xlsx_) o en almacenamiento remoto
(en la nube) en formato de hojas de datos de Google Sheets. Para esto último es necesario configurar el acceso a Google 
Drive a través de la API de Google (ver tutorial abajo).


### Programar pasar lista automáticamente al finalizar cada clase (solo Linux y MacOs)

La API genera un _script_ de Python `pasarLista.py` que, en sistemas Linux o MacOS, puede ejecutarse por ventana de 
comandos, de modo que la API ofrece agregarlo a `crontab` para que se ejecute automáticamente después cada clase 
(requiere tener la PC encendida y la sesión de usuario iniciada).


## Instalación
```
pip install git+https://gitlab.com/axelmlacap/ipchelper
```

---

## Guía de uso

La API provee una clase `Course` que almacena la información de un curso de IPC junto con métodos útiles para asistir a 
las tareas del docente.

El método `Course.create` permite crear un curso a partir de un único archivo de configuración:

```commandline
from ipchelper import Course

course = Course.create("initFile.yaml")
```

El archivo de configuración `initFile.yaml` es un archivo de texto en formato YAML[^1] que define una serie de 
propiedades del curso y un conjunto de rutas o enlaces a archivos necesarios para funcionar.

[^1]: YAML es un lenguaje de serialización de datos legible para humanos. El soporte de YAML en Python está permitido 
por la librería externa [PyYAML](https://pypi.org/project/PyYAML/), que permite convertir diccionarios a archivos YAML 
y viceversa.

---

### Configuración

**Ejemplo de archivo de configuración en formato YAML:**
```commandline
name: IPC, 1er semestre 2024  # nombre del curso
schedule:                     # horarios de clase
  - lun 8:00
  - jue 9:40
startDate: 26/02/24           # fecha de primera clase
weeks: 20                     # cantidad de semanas del curso
classLength: 90               # duración de la clase [min]
tolerance: 20                 # margen de tolerancia para respuestas [min]
keywords: frutas              # palabras clave
paths:                        # archivos necesarios:
  students: alumnos.xlsx      #   tabla de alumnos
  input: respuestas.xlsx      #   lista de respuestas al formulario
  output: planilla.xlsx       #   planilla del curso
  form: https://docs.google.com/forms/d/e/1FAIpQLScKJ0B1AW2EwF2xosNjv5Du-AhA5DEr0AVdLFJ748yJH50zjg  #   formulario de Google
```
- `name` (`str`, opcional): nombre del curso. (Por defecto: `"Mi Curso"`.) 
- `schedule` (`List[str]`, opcional): horarios del curso, especificados como una lista de strings (uno por cada día de 
clase en la semana) indicando día de la semana y horario de inicio de clase (en formato `ddd hh:mm`). (Por defecto: 
`["lun 8:00", "mar 8:00"]`.)
- `startDate` (`str`, opcional) fecha de la primera clase del curso (en formato `dd/mm/aa`, aunque esto puede 
configurarse). (Por defecto: toma la fecha de hoy como inicio de clases.)
- `weeks` (`int`): cantidad de semanas que durará el curso. (Por defecto: 20.)
- `classLength` (`int`): duración de cada clase (en minutos). (Por defecto: 90.)
- `tolerance` (`int`): margen de tiempo fuera del horario de clases en el que se aceptarán respuestas al formulario de 
asistencia (en minutos). (Por defecto: 20.) 
- `keywords` (`List[str]`, `str` ó `pathlib.Path`): lista de palabras clave que se utilizará en el formulario de 
asistencia, especificado como:
  - `str`: un _string_ indicando alguno de los conjuntos preestablecidos de palabras claves (debe ser una clave del 
  diccionario `ipchelper.KEYWORDS`).
  - `List[str]`: la lista explícita de palabras clave a usar. Para cada clase se elegirá uno de estos _strings_ al azar
  (elección al azar con repetición).
  - `str` ó `pathlib.Path`: la ruta a un archivo _.csv_, _.xls_ o _.xlsx_ que defina las fechas de cada clase y sus 
  palabras claves [^2].
- `paths` (`Dict[str, pathlib.Path]`): diccionario de rutas (direcciones o enlaces a archivos) necesarias para la 
inicialización y el funcionamiento del Asistente. Cada clave corresponde a un archivo con una funcionalidad distinta, 
algunos valores son obligatorios para que el Asistente funcione, otros son opcionales. Las claves posibles son:
  - `input` (obligatorio): hoja de datos con las respuestas al formulario de Google[^3]. De aquí se leerán las 
  respuestas de los estudiantes para tomar asistencia. 
  - `output` (obligatorio): archivo Excel con la planilla del curso[^4]. Aquí se escribirá el resultado de procesar las 
  respuestas al formulario.
  - `students` (opcional): hoja de datos con la tabla de alumnos[^5]. De aquí se leerán los datos de los alumnos (debe 
  ser un archivo generado desde la propia clase `Course`, por ejemplo, mediante `Course.export`).
  - `gestion` y `campus` (opcional): hoja de datos con las tablas de alumnos provistas por el sistema de Gestión y por 
  el Campus respectivamente. Si no se especifica un archivo `students`, la tabla de alumnos es generada leyendo los 
  datos en estos archivos a través del método `Course.initStudents`. Notar que el archivo `campus` es opcional pero 
  necesario para tener los emails de los alumnos, de otra manera deberán agregarse a mano en el archivo `gestion`.
  - `form` (opcional): enlace al formulario de asistencia mediante el cual se reciben las respuestas de los alumnos. 
  Se generarán imágenes para mostrar en clase un QR que dirige a esta dirección para que los alumnos accedan.
  - `cred` (opcional): credencial JSON de autenticación para el acceso a la API de Google Drive. Necesaria si algunos 
  de los archivos se encuentran en la nube.

[^2]: Las columnas del archivo deben estar en el orden asignado en `course.colOrder['keywords']'`. La columna `keyword` 
define la palabra clave de cada clase, de modo que la propiedad `keywords` se toma como el _set_ de palabras claves sin 
repetir, ordenadas alfabéticamente. (Notar que las columnas del archivo no tienen que respetar esos nombres ya que 
solo se utilizan las posiciones de las columnas, nunca sus nombres.)

[^3]: Las columnas del archivo deben estar en el orden asignado en `course.colOrder['students']'`. La columna `email`
será utilizada para identificar al alumno a partir de las respuestas. Las columnas cuyo nombre empiezan con `_` no 
tienen una funcionalidad establecida.

[^4]: El archivo debe contener una hoja (cuyo nombre está definido en `course.sheetNames['attendance']`) que almacene 
la asistencia. El método `Course.createSheets` permite crear esta hoja automáticamente, junto con otras que almacenan 
las calificaciones, los datos de los alumnos y la lista de palabras claves.

[^5]: El archivo debe contener una hoja cuyo nombre está definido en `course.sheetNames['students']` y sus columnas 
deben estar en el orden definido en `course.colOrder['students']'`. La columna `email` será utilizada para identificar 
al alumno a partir de las respuestas, mientras que las columnas cuyo nombre empiezan con `_` no tienen una 
funcionalidad establecida. (Notar que las columnas del archivo no tienen que respetar esos nombres ya que 
solo se utilizan las posiciones de las columnas, nunca sus nombres.)

---

### Ejecución

El método `Course.create` crea el curso de la siguiente manera:

1. Carga las propiedades definidas en el archivo de configuración.

2. Inicializa un curso con esas propiedades. Aquí se computan las fechas de todas las clases y se les asigna una 
palabra clave.

3. Si en el archivo de configuración está definida la ruta `students`, la tabla de alumnos se carga de ahí. Si no, se 
inicializa una nueva tabla a partir de las hojas de datos `gestion` y `campus`.

4. Crea la planilla del curso: un archivo Excel con cuatro hojas de datos[^6] inicializadas para almacenar la tabla de 
calificaciones, la tabla de asistencia, la tabla de alumnos y la tabla de palabras claves respectivamente. La planilla 
se escribe en la ruta `output` del archivo de configuración (si el archivo existe, su contenido se sobrescribe).

5. Crea un conjunto de imágenes _.png_ para proyectar una en cada clase la palabra clave asignada a ese día junto con 
un código QR que lleva al formulario de asistencia del curso[^7].

6. Crea un script para tomar asistencia llamado `pasarLista.py` que lee la tabla de respuestas y actualiza la planilla 
del curso. En Linux y MacOS: este archivo se puede ejecutar por ventana de comandos, y además se permite añadir el 
script a `crontab` para que se ejecute automáticamente 30 minutos después de finalizar cada clase[^8].

7. Por defecto el curso se exporta en el directorio de trabajo del intérprete de Python que ejecuta el método 
`Course.create`. Alternativamente se puede asignar un directorio distinto mediante el argumento opcional `saveTo`:
```
course = Course.create("initFile.yaml", saveTo="mi_curso/")
```
(Notar que `saveTo` debe especificar la ruta a **una carpeta** en la cual se almacenarán los datos del curso necesarios 
para volver a cargarlo mediante el método `Course.load`. Si la carpeta no existe, la crea. Si exsite y contiene 
archivos con el mismo nombre que los que se van a guardar, los sobrescribe.)

El _script_ `pasarLista.py` está pensado para ejecutarse después de cada clase: se encarga de cargar los datos del 
curso, leer las respuestas del formulario, computar las asistencias y actualizar la planilla del curso.

[^6]: El nombre de cada hoja de datos está definido en `course.sheetNames['grades']`, `course.sheetNames['attendance']`, 
`course.sheetNames['students']` y `course.sheetNames['keywords']` respectivamente.

[^7]: La URL del formulario de asistencia debe especificarse en la ruta `form` del archivo de configuración.

[^8]: Esto se logra mediante el argumento opcional `crontab` del método `Course.create`:
```commandline
course = Course.create("initFile.yaml", saveTo="mi_curso/", crontab=True)
```

---

## Uso de almacenamiento remoto

La API utiliza [gspread](https://docs.gspread.org/) para leer y escribir hojas de datos almanceadas en Google Drive a 
través de la API de Google.

### ¿Cómo habilitar el acceso a la API de Google?
(Fuente: [documentación de GSpreads](https://docs.gspread.org/en/v6.0.0/oauth2.html#enable-api-access))

1. Ir a [Google Developers Console](https://console.cloud.google.com/apis/dashboard) y hacer click en el botón _+ 
ENABLE APIS AND SERVICES_.

2. En el recuadro de búsqueda _Buscar APIs y servicios_ (_Search for APIs and services_), buscar "Google Drive API” y 
habilitarlo.

3. En el recuadro de búsqueda _Buscar APIs y servicios_ (_Search for APIs and services_), buscar "Google Sheets API” y 
habilitarlo.

4. **Crear una cuenta de servicio**. Para ello, presionar el botón _Crear credenciales_ (_Create credential_) y elegir 
_Cuenta de Servicio_ (_Service Account_). Llenar el formulario, presionar _Crear_ (_Create_) y _Listo_ (_Done_).

5. En la solapa _Cuentas de servicio_ (_Service Accounts_)', ingresar a [_Administrar Cuentas de Servicio_ (_Manage 
Service Accounts_)](https://console.cloud.google.com/iam-admin/serviceaccounts?)

6. **Crear una nueva clave**. Para ello, en la columna _Acciones_ (_Actions_) presionar los tres puntos verticales (⋮) 
de la cuenta de servicio creada recientemente e ingresar a _Administrar claves_ (_Manage keys_). Presionar _Agregar 
clave_ (_Add key_) > _Crear clave nueva_ (_Add new key_). Elejir _JSON_ y presionar _Crear_ (_Create_). Se descargará 
un archivo JSON con un contenido parecido a:
```
{"type": "service_account",
 "project_id": "api-project-XXX",
 "private_key_id": "2cd … ba4",
 "private_key": "-----BEGIN PRIVATE KEY-----\nNrDyLw … jINQh/9\n-----END PRIVATE KEY-----\n",
 "client_email": "473000000000-yoursisdifferent@developer.gserviceaccount.com",
 "client_id": "473 … hd.apps.googleusercontent.com",
 ...}
```
**Importante**: hay que recordar la ubicación del archivo descargado ya que al inicializar el Asistente habrá que 
asignar esa ubicación al archivo `cred`. Además vamos a necesitar el valor del campo `client_email` en el paso 
siguiente.

7. **Desde Google Drive**: compartir todos los archivos necesarios con la cuenta del campo `client_email` del paso 
anterior. Caso contrario, recibiremos un error `gspread.exceptions.SpreadsheetNotFound` \
**Importante**: asignarle permisos de **editor** a la tabla de respuestas (ruta `input`) y a la planilla del curso 
(ruta `output`).

### ¿Cómo configurar el Asistente para utilizar hojas de datos en la nube?

1. Para que el Asistente pueda acceder a cualquier archivo de Google Drive hay que compartirlo con permisos de 
**editor** (aún si solo vamos a leer su contenido y no queremos modificarlo).

2. Luego basta con modificar el archivo de configuración e ingresar la URL en la ruta correspondiente del archivo de 
configuración. Por ejemplo, podríamos tener un archivo de configuración con las siguientes rutas:
```commandline
paths:
  campus: campus.xlsx  # este es un archivo local
  gestion: gestion.xlsx  # este también
  input: https://docs.google.com/spreadsheets/d/1q8IBvVHiTofKCu9wq0Bhhxd-V-w5-pWnXu-f5rZCfGY  # este es un archivo remoto
  output: https://docs.google.com/spreadsheets/d/1GRR79quRMP8i1uGAKOa0wsUVkqvDMurQopFOWxDrOjs  # este también
  form: https://docs.google.com/forms/d/e/1FAIpQLScKJ0B1AW2EwF2xosNjv5Du-AhA5DEr0AVdLFJ748yJH50zjg
  cred: credencial.json
```

Notar que `cred` debe ser la ruta local de la credencial JSON descargada en el paso 6 de la sección anterior.

---